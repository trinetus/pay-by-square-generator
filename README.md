# PayBySquareGenerator

This is PHP implementation of Pay By Square generator. Source code is based on code of **Ján Fečík** (https://jan.fecik.sk/blog/qr-generator-platieb-pay-by-square-v-php/) 
and ported as composer package and spiced with tiny OOP.

This library **not requires** registration on **api.


## Requirements

- PHP 7.4+
- Enabled function **proc_open()** on server
- Linux server
- Installed **xz** package on server *(for LZMA compression, which is necessary for correct compression of data during generation)*


## Installation

```bash
composer require trinetus/pay-by-square-generator
```


## Usage

Simply add namespace usage to your code:

```php
use Trinetus\PayBySquareGenerator;
```

Initialize object, fill with invoice data and return final output string:

```php
$outputString = (new PayBySquareGenerator())
    ->setAmount(1.25)
    ->setIban("SKxxyyyyzzzzzzzzzzzzzzzz")
    ->setBic("XXXXXXXX")
    ->setBeneficaryName("Account name")
    ->setVariableSymbol("1234567890")
    ->setConstantSymbol("0309")
    ->setSpecificSymbol("9999")
    ->setNote("Test payment")
    ->getOutput();
```

Use some of your favorite QR code generator for creating QR code from `$outputString`. This example is using **bacon/bacon-qr-code** (https://packagist.org/packages/bacon/bacon-qr-code)

```php
$renderer = new ImageRenderer(
    new RendererStyle(400),
    new ImagickImageBackEnd()
);
$writer = new Writer($renderer);
$qrcode = $writer->writeString($outputString);
```

Simply display image in your web/pdf/email template (or do whatever you need with QR code image). This example is for Laravel's blade template:

```php
<img src="data:image/png;base64,{{ base64_encode($qrcode) }}" alt="QR code">
```

Or you can also save it as image file (see docs for your QR code generator library).


## Sources and useful links

- Main source:
  https://jan.fecik.sk/blog/qr-generator-platieb-pay-by-square-v-php/

- XSD schema docs + link: 
  https://www.bsqr.co/schema/
