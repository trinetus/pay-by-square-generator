<?php

namespace Trinetus\PayBySquareGenerator;

class PayBySquareGenerator
{
    private float $amount = 0.00;
    private string $currency = "EUR";
    private string $date;
    private string $variableSymbol;
    private string $constantSymbol;
    private string $specificSymbol;
    private string $paymentReference;
    private string $note;
    private string $iban;
    private string $bic;
    private string $beneficaryName;
    private string $beneficaryAddress1;
    private string $beneficaryAddress2;


    public function setAmount(float $amount): self
    {
        $this->amount = $amount;
        return $this;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;
        return $this;
    }

    public function setDate(\DateTime $date): self
    {
        $this->date = $date->format("Ymd");
        return $this;
    }

    public function setVariableSymbol(string $vs): self
    {
        $this->variableSymbol = $vs;
        return $this;
    }

    public function setConstantSymbol(string $cs): self
    {
        $this->constantSymbol = $cs;
        return $this;
    }

    public function setSpecificSymbol(string $ss): self
    {
        $this->specificSymbol = $ss;
        return $this;
    }

    public function setPaymentReference(string $paymentRef): self
    {
        $this->paymentReference = $paymentRef;
        return $this;
    }

    public function setNote(string $note): self
    {
        $this->note = $note;
        return $this;
    }

    public function setIban(string $iban): self
    {
        $this->iban = $iban;
        return $this;
    }

    public function setBic(string $bic): self
    {
        $this->bic = $bic;
        return $this;
    }

    public function setBeneficaryName(string $beneficaryName): self
    {
        $this->beneficaryName = $beneficaryName;
        return $this;
    }

    public function setBeneficaryAddress1(string $beneficaryAddress1): self
    {
        $this->beneficaryAddress1 = $beneficaryAddress1;
        return $this;
    }

    public function setBeneficaryAddress2(string $beneficaryAddress2): self
    {
        $this->beneficaryAddress2 = $beneficaryAddress2;
        return $this;
    }

    public function getOutput(): string
    {
        $dataStr = $this->getDataStr();
        $crcWithData = $this->getDataCrc($dataStr) . $dataStr;

        $compressed = $this->lzma1Compress($crcWithData);

        $hexData = $this->convertToHex($compressed, strlen($crcWithData));

        return $this->padAndTranslate($hexData);
    }

    private function getDataStr(): string
    {
        return implode("\t", [
            0 => '',
            1 => '1',
            2 => implode("\t", [
                "1",
                $this->amount ?? 0.01,
                $this->currency ?? "EUR",
                $this->date ?? (new \DateTime())->format("Ymd"),
                $this->variableSymbol ?? "",
                $this->constantSymbol ?? "",
                $this->specificSymbol ?? "",
                $this->paymentReference ?? "",
                $this->note ?? "",
                "1",
                $this->iban ?? "",
                $this->bic ?? "",
                "0",
                "0",
                $this->beneficaryName ?? "",
                $this->beneficaryAddress1 ?? "",
                $this->beneficaryAddress2 ?? ""
            ])
        ]);
    }

    private function getDataCrc(string $data): string
    {
        return strrev(hash("crc32b", $data, TRUE));
    }

    private function lzma1Compress(string $data): string
    {
        $x = proc_open("/usr/bin/xz '--format=raw' '--lzma1=lc=3,lp=0,pb=2,dict=128KiB' '-c' '-'", [0 => ["pipe", "r"], 1 => ["pipe", "w"]], $p);
        fwrite($p[0], $data);
        fclose($p[0]);

        $output = stream_get_contents($p[1]);
        fclose($p[1]);
        proc_close($x);

        return $output;
    }

    private function convertToHex(string $compressedData, int $dataLen): string
    {
        return bin2hex("\x00\x00" . pack("v", $dataLen) . $compressedData);
    }

    private function padAndTranslate(string $data): string
    {
        $len = strlen($data);
        $b = "";
        for ($i = 0; $i < $len; $i++) {
            $b .= str_pad(base_convert($data[$i], 16, 2), 4, "0", STR_PAD_LEFT);
        }

        $l = strlen($b);
        $r = $l % 5;
        if ($r > 0) {
            $p = 5 - $r;
            $b .= str_repeat("0", $p);
            $l += $p;
        }

        $l = $l / 5;
        $data = str_repeat("_", $l);

        for ($i = 0; $i < $l; ++$i) {
            $data[$i] = "0123456789ABCDEFGHIJKLMNOPQRSTUV"[bindec(substr($b, $i * 5, 5))];
        }

        return $data;
    }
}